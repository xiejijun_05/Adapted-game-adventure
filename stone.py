import param
import pygame
import random

# 石头障碍物
# 点以50为单位
class Stone:
    def __init__(self):
        self.body = []
        self.allStoneHeigh = []

        for y in range(125, param.HALF_Y - 50, param.STONE_WIDTH):
            self.allStoneHeigh.append(y)

        for x in range(0, param.SCREEN_X, 200):
            self.body.append(self.generateWall(x, True))
            self.body.append(self.generateWall(x, False))

    def addOne(self):
        self.body.insert(0, self.generateWall(
            param.SCREEN_X - param.STONE_WIDTH, True))
        self.body.insert(0, self.generateWall(
            param.SCREEN_X - param.STONE_WIDTH, False))

    def generateWall(self, left, isTop):
        heigh = random.choice(self.allStoneHeigh)
        top = 0
        if isTop:
            top = 0
        else:
            top = param.SCREEN_Y - heigh

        wall = []
        for i in range(0, heigh, 50):
            wall.append(pygame.Rect(left, top + i,
                                    param.STONE_WIDTH, param.STONE_WIDTH))
        return wall

    def movePixel(self, pixel):
        print(len(self.body))
        if len(self.body) > 0:
            for wall in self.body:
                for stone in wall:
                    stone.left -= pixel
            if len(self.body) > 12:
                self.body.pop()

    def isContain(self, left, top):
        for wall in self.body:
            for stoneRect in wall:
                if (stoneRect.left == left or stoneRect.left == left - 25) and (stoneRect.top == top or stoneRect.top == top - 25):
                    return True

        return False
