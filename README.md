#### 作品名
贪食蛇捕蛙

#### 项目简介
这是一个基于贪吃蛇捕蛙的游戏，贪食蛇吃了青蛙并不会变长，当贪食蛇吃了一些青蛙之后，游戏关卡会升级，升级后蛇的行动速度会变快，同时，障碍物也会快速移动。蛇不可以触碰障碍物，即使是身体也不行，所以在吃障碍物直接的青蛙时需要额外小心。贪食蛇捕蛙配备了节奏感极强的背景音乐及捕蛙和死亡音效，赶紧体验一下吧，看看你能坚持几关，一定会让你爱不释手的。

#### 游戏截图（1-3张）

![输入图片说明](https://images.gitee.com/uploads/images/2020/0806/200032_1d4129c6_340587.png )

![截图2](https://images.gitee.com/uploads/images/2020/0806/195848_b79ef58e_340587.png "2.png")

![游戏结束](https://images.gitee.com/uploads/images/2020/0806/195932_dc288723_340587.png "3.png")

#### 项目成员
  
团队：
团队成员
- 队长： 谢**(微信号：xi**60)
- 队员：
    - 谢**(微信号：xi**60)
    - 商**(微信号：yu**ng)

#### 项目地址

https://gitee.com/xiejijun_05/Adapted-game-adventure

#### 视频地址

https://www.bilibili.com/video/BV1zA411Y7pk/