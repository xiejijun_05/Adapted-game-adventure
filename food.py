import param
import pygame
import random


# 食物类
# 方法： 放置/移除
# 点以25为单位
class Food:
    def __init__(self):
        self.rect = pygame.Rect(-25, 0, 25, 25)
        self.allposX = []
        self.allposY = []
        for pos in range(25, param.SCREEN_X - 25, 25):
            self.allposX.append(pos)
        for pos in range(25, param.SCREEN_Y - 25, 25):
            self.allposY.append(pos)

    def remove(self):
        self.rect.x = -25

    def set(self, stone):
        if self.rect.x <= -25:
            allpos = []
            # 不靠墙太近 25 ~ SCREEN_X-25 之间
            for pos in range(25, param.SCREEN_X - 25, 25):
                allpos.append(pos)

            self.rect.left = random.choice(self.allposX)
            self.rect.top = random.choice(self.allposY)

            if stone.isContain(self.rect.left, self.rect.top):
                self.rect.left = -25
                self.set(stone)

    def move(self):
        self.rect.x -= 25
