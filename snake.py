import pygame
import random
import param
from stone import Stone

# 蛇类
# 点以25为单位
class Snake(object):
    # 初始化各种需要的属性 [开始时默认向右/身体块x5]
    def __init__(self):
        self.dirction = pygame.K_RIGHT
        self.body = []
        for x in range(5):
            self.addnode()

    # 无论何时 都在前端增加蛇块
    def addnode(self):
        left, top = (0, param.HALF_Y)
        if self.body:
            left, top = (self.body[0].left, self.body[0].top)
        node = pygame.Rect(left, top, 25, 25)
        if self.dirction == pygame.K_LEFT:
            node.left -= 25
        elif self.dirction == pygame.K_RIGHT:
            node.left += 25
        elif self.dirction == pygame.K_UP:
            node.top -= 25
        elif self.dirction == pygame.K_DOWN:
            node.top += 25
        self.body.insert(0, node)

    # 删除最后一个块
    def delnode(self):
        self.body.pop()

    # 死亡判断
    def isdead(self, stone):
        # 撞墙
        if self.body[0].x not in range(param.SCREEN_X):
            return True
        if self.body[0].y not in range(param.SCREEN_Y):
            return True
        # 撞自己
        if self.body[0] in self.body[1:]:
            return True

        # 撞障碍物
        for rect in self.body:
            if stone.isContain(rect.left, rect.top):
                return True
        return False

    # 移动！
    def move(self):
        self.addnode()
        self.delnode()

    # 改变方向 但是左右、上下不能被逆向改变
    def changedirection(self, curkey):
        LR = [pygame.K_LEFT, pygame.K_RIGHT]
        UD = [pygame.K_UP, pygame.K_DOWN]

        if curkey == self.dirction:
            self.move()

        if curkey in LR + UD:
            if (curkey in LR) and (self.dirction in LR):
                return
            if (curkey in UD) and (self.dirction in UD):
                return
            self.dirction = curkey

    def deadAction(self, screen, clock):
        failedImg1 = pygame.image.load('res/tail_up.png')
        failedImg2 = pygame.image.load('res/tail_right.png')
        failedImg3 = pygame.image.load('res/tail_down.png')
        failedImg4 = pygame.image.load('res/tail_left.png')
        imgList = [failedImg1, failedImg2, failedImg3, failedImg4]
        count = 0
        for img in imgList:
            count += 1
            if count >= 4:
                break
            screen.blit(img, (350, 300))
            clock.tick(2)
            pygame.display.update()
