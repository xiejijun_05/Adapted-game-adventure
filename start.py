#!/bash/bin
# -*- coding:utf-8 -*-
import pygame
import sys
import random
import logging
import param
from stone import Stone
from food import Food
from snake import Snake


def show_text(screen, pos, text, color, font_bold=False, font_size=60, font_italic=False):
    # 获取系统字体，并设置文字大小
    cur_font = pygame.font.SysFont("宋体", font_size)
    # 设置是否加粗属性
    cur_font.set_bold(font_bold)
    # 设置是否斜体属性
    cur_font.set_italic(font_italic)
    # 设置文字内容
    text_fmt = cur_font.render(text, 1, color)
    # 绘制文字
    screen.blit(text_fmt, pos)


def getLeve(score):
    level = 0
    for item in param.LEVEL_SCORE:
        if score < item:
            break
        level += 1
    return level


def main():
    pygame.init()
    pygame.mixer.init()
    pygame.mixer.music.load('res/bg-music.mp3')
    pygame.mixer.music.set_volume(0.2)
    pygame.mixer.music.play(1, 0.0)

    eatSound = pygame.mixer.Sound('res/coin.wav')
    eatSound.set_volume(2)

    failedSound = pygame.mixer.Sound('res/failed.wav')
    failedSound.set_volume(2)

    screen_size = (param.SCREEN_X, param.SCREEN_Y)
    screen = pygame.display.set_mode(screen_size)
    pygame.display.set_caption('贪食蛇捕蛙')
    clock = pygame.time.Clock()
    scores = 0
    isdead = False
    level = param.MIN_LEVEL
    isShowDead = True

    # 蛇/食物
    snake = Snake()
    food = Food()
    stone = Stone()

    foodImg = pygame.image.load('res/timg.png')
    foodImg = pygame.transform.smoothscale(foodImg, (25, 25))

    snakeHeadImg = pygame.image.load('res/snakeHead.png')
    snakeHeadImg = pygame.transform.smoothscale(snakeHeadImg, (25, 25))

    snakeBodyImg = pygame.image.load('res/snakeBody.png')
    snakeBodyImg = pygame.transform.smoothscale(snakeBodyImg, (25, 25))

    snakeTailLeftImg = pygame.image.load('res/tail_left.png')
    snakeTailLeftImg = pygame.transform.smoothscale(snakeTailLeftImg, (25, 25))

    snakeTailRightImg = pygame.image.load('res/tail_right.png')
    snakeTailRightImg = pygame.transform.smoothscale(
        snakeTailRightImg, (25, 25))

    snakeTailUpImg = pygame.image.load('res/tail_up.png')
    snakeTailUpImg = pygame.transform.smoothscale(snakeTailUpImg, (25, 25))

    snakeTailDownImg = pygame.image.load('res/tail_down.png')
    snakeTailDownImg = pygame.transform.smoothscale(snakeTailDownImg, (25, 25))

    stoneImg = pygame.image.load('res/wall.png')
    stoneImg = pygame.transform.smoothscale(stoneImg, (50, 50))

    tick = 0
    while True:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            if event.type == pygame.KEYDOWN:
                if not isdead:
                    snake.changedirection(event.key)
                # 死后按space重新
                if event.key == pygame.K_SPACE and isdead:
                    isShowDead = True
                    return main()

        screen.fill((34, 59, 84))

        # 画蛇身 / 每一步+1分
        if not isdead:
            snake.move()

        isHead = True
        idx = 0
        lastRect = None
        for rect in snake.body:
            idx += 1
            if isHead:
                # head
                isHead = False
                screen.blit(snakeHeadImg, rect)
            elif idx == len(snake.body):
                # tail
                if rect.left == lastRect.left:
                    if rect.top > lastRect.top:
                        # down
                        screen.blit(snakeTailDownImg, rect)
                    else:
                        # up
                        screen.blit(snakeTailUpImg, rect)
                elif rect.top == lastRect.top:
                    if rect.left > lastRect.left:
                        # right
                        screen.blit(snakeTailRightImg, rect)
                    else:
                        # left
                        screen.blit(snakeTailLeftImg, rect)

            else:
                # body
                screen.blit(snakeBodyImg, rect)
                lastRect = rect

        tick += 1
        # 画障碍物
        if not isdead:
            if tick % 4 == 0:
                stone.movePixel(25)
                food.move()

            if tick % 32 == 8:
                stone.addOne()

        for rectList in stone.body:
            for rect in rectList:
                screen.blit(stoneImg, rect)

        # 食物投递
        food.set(stone)
        screen.blit(foodImg, food.rect)

        # 食物处理 / 吃到+10分
        # 当食物rect与蛇头重合, 吃掉
        if food.rect == snake.body[0]:
            eatSound.play()
            scores += 10
            food.remove()
            # snake.addnode() 不要变长

        # 显示分数文字和等级
        level = getLeve(scores)
        show_text(screen, (50, 50), 'Scores: ' +
                  str(scores), (22, 22, 22), False, 50)

        show_text(screen, (50, 100), 'Level: ' +
                  str(level), (22, 22, 22), False, 50)

        # 显示死亡文字
        isdead = snake.isdead(stone)
        # print("isdead="+str(isdead)+";isShowDead="+str(isShowDead))
        if isdead:
            pygame.mixer.music.stop()
            if isShowDead:
                failedSound.play()
                snake.deadAction(screen, clock)
                isShowDead = False
            else:
                show_text(screen, (250, 250), 'DEFEAT!',
                          (27, 229, 18), False, 100)
                show_text(screen, (250, 310),
                          'Hero, try again? press [space key].', (0, 0, 22), False, 30)

        pygame.display.update()
        clock.tick(5 * level)

    # end while


if __name__ == '__main__':
    main()
